<?php
$page_title = 'Admin página de inicio';
require_once('../assets/includes/load.php');
if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
page_require_level_modules(9);
?>
<?php
 $c_categorie     = count_by_id('categories');
 $c_product       = count_by_id('products');
 $c_sale          = count_by_id('sales');
 $c_user          = count_by_id('users');
 $products_sold   = find_higest_saleing_product('10');
 $recent_products = find_recent_product_added('5');
 $recent_sales    = find_recent_sale_added('5')
?>

<?php include_once('../assets/layouts/headersub.php'); 

date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i:s'); ?>

<div id="divXCambiar">

<?php 
  include('contenido.php'); 
 ?>

</div>

<?php include_once('../assets/layouts/footersub.php'); ?>
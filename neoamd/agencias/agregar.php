<?php
$page_title = 'Agregar Grupo';
require_once('../assets/includes/load.php');
 // Checar cual es el nivel permitido de usuario
   page_require_level(1);
?>

<?php
  if(isset($_POST['add'])){

   $req_fields = array('group-name','group-level');
   validate_fields($req_fields);

   if(find_by_groupName($_POST['group-name']) === false ){
     $session->msg('d','<b>Error!</b> El nombre de grupo ya existe en la base de datos');
     redirect('agregar.php', false);
   }elseif(find_by_groupLevel($_POST['group-level']) === false) {
     $session->msg('d','<b>Error!</b> El nivel de grupo ya existe en la base de datos ');
     redirect('agregar.php', false);
   }
   if(empty($errors)){
           $name = remove_junk($db->escape($_POST['group-name']));
          $level = remove_junk($db->escape($_POST['group-level']));
         $status = remove_junk($db->escape($_POST['status']));

        $query  = "INSERT INTO user_groups (";
        $query .="group_name,group_level,group_status";
        $query .=") VALUES (";
        $query .=" '{$name}', '{$level}','{$status}'";
        $query .=")";
        if($db->query($query)){
          //sucess
          $session->msg('s',"Grupo ha sido creado! ");
          redirect('agregar.php', false);
        } else {
          //failed
          $session->msg('d','Lamentablemente no se pudo crear el grupo!');
          redirect('agregar.php', false);
        }
   } else {
     $session->msg("d", $errors);
      redirect('agregar.php',false);
   }
 }
?>

<?php include_once('../assets/layouts/headersub.php'); 
date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i a'); ?>

<div id="divXCambiar">
<?php 
  include('agregarcont.php'); 
 ?>
</div>

<?php include_once('../assets/layouts/footersub.php'); ?>
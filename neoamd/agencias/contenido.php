  <!-- Hero -->
  <div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Agencias <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Administración de Agencias del Grupo.</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Accesos</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Administrar Agencias</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">

    <div class="row">
         <div class="col-md-12">
           <?php echo display_msg($msg); ?>
       </div>
       <div class="col-md-10"></div>
       <div align="right" class="col-md-2">
            <a href="agregar.php" class="btn btn-success"> Agregar AGENCIA</a>
            
       </div>
    </div>

    <br>

    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Agencias <small>Existentes</small></h3>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th class="d-none d-sm-table-cell" style="width: 20%;">Marca</th>
                        <th class="d-none d-sm-table-cell" style="width: 25%;">Nombre</th>
                        <th class="d-none d-sm-table-cell" style="width: 20%;">Ciudad</th>
                        <th class="d-none d-sm-table-cell" style="width: 25%;">Empresa</th>
                      
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Estado</th>
                        <th style="width: 15%;">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($all_agencys as $a_agency): ?>
                        <tr>
                            <td class="text-center font-size-sm"><?php echo count_id();?></td>
                           
                            <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_agency['marca']))?>
                            </td>
                             <td class="font-w600 font-size-sm">
                                <a target="_blank" href="<?php echo $a_agency['url']?>" > <?php echo remove_junk(ucwords($a_agency['nombre'])) ?>  </a>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_agency['ciudad']))?>
                            </td>
                              <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_agency['razon']))?>
                            </td>
                             
                            <td class="d-none d-sm-table-cell">
                                <?php if($a_agency['status'] === '1'): ?></span>
                                    <span class="badge badge-info"><?php echo "Activo"; ?></span>
                                <?php else: ?>
                                    <span class="badge badge-danger"><?php echo "Inactivo"; ?></span>
                                <?php endif;?>
                            </td>
                            <td>
                                <a href="editar.php?id=<?php echo (int)$a_group['id'];?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Editar">
                                    <i class="fas fa-pencil-alt fa-1x"></i>
                                </a>

                                <a href="eliminar.php?id=<?php echo (int)$a_group['id'];?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                    <i class="fas fa-trash-alt fa-1x"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->
<?php
  $page_title = 'Editar Grupo';
  require_once('../assets/includes/load.php');
  // Checar cual es el nivel permitido de usuario
   page_require_level(1);

   $e_group = find_by_id('user_groups',(int)$_GET['id']);
  if(!$e_group){
    $session->msg("d","Missing Group id.");
    redirect('index.php');
  }
?>
 
<?php
  if(isset($_POST['update'])){

   $req_fields = array('group-name','group-level');
   validate_fields($req_fields);
   if(empty($errors)){
           $name = remove_junk($db->escape($_POST['group-name']));
          $level = remove_junk($db->escape($_POST['group-level']));
         $status = remove_junk($db->escape($_POST['val-select2-edigpr']));

        $query  = "UPDATE user_groups SET ";
        $query .= "group_name='{$name}',group_level='{$level}',group_status='{$status}'";
        $query .= "WHERE ID='{$db->escape($e_group['id'])}'";
        $result = $db->query($query);
         if($result && $db->affected_rows() === 1){
          //sucess
          $session->msg('s',"Grupo se ha actualizado! ");
          redirect('index.php?id='.(int)$e_group['id'], false);
        } else {
          //failed
          $session->msg('d','Lamentablemente no se ha actualizado el grupo!');
          redirect('editar.php?id='.(int)$e_group['id'], false);
        }
   } else {
     $session->msg("d", $errors);
    redirect('editar.php?id='.(int)$e_group['id'], false);
   }
 }
?>

<?php include_once('../assets/layouts/headersub.php'); 
date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i a'); ?>

<div id="divXCambiar">
<?php 
  include('editarcont.php'); 
 ?>
</div>

<?php include_once('../assets/layouts/footersub.php'); ?>




<!-- Hero -->
<div class="bg-body-light">
	<div class="content content-full">
		<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
			<h1 class="flex-sm-fill h3 my-2">
				Agregar Grupo <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Agregar un nuevo grupo de usuarios.</small>
			</h1>
			<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-alt">
					<li class="breadcrumb-item">Accesos</li>
					<li class="breadcrumb-item" aria-current="page">
						<a class="link-fx" href="">Agregar Grupo</a>
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">

	 <div class="row">
         <div class="col-md-12">
           <?php echo display_msg($msg); ?>
       </div>
        
</div>

    <br>

	<!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
	<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
	<form class="js-validation" action="agregar.php" method="POST">
		<div class="block">
			<div class="block-header">
				<h3 class="block-title">Agregar GRUPO</h3>
			</div>
			<div class="block-content block-content-full">
				<div class="">
					<!-- Regular -->
					<h2 class="content-heading border-bottom mb-4 pb-2">Llena los siguientes datos.</h2>
					<div class="row items-push">
						<div class="col-lg-4">
							<p class="font-size-sm text-muted">
								Los grupos son comunidades o niveles en los usuarios, Entre mas bajo es el nivel mayores son los privilegios.
							</p>
						</div>
						<div class="col-lg-8 col-xl-5">
							<div class="form-group">
								<label for="group-name">Nombre de Grupo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="group-name" name="group-name" placeholder="Ingresa un Nombre..">
							</div>
							<div class="form-group">
								<label for="group-level">Nivel <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="group-level" name="group-level" placeholder="Ingresa un valor">
							</div>
							<div class="form-group">
								<label for="val-select2-addgpr">Estado <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="val-select2-addgpr" name="val-select2-addgpr" style="width: 100%;" data-placeholder="Selecciona uno..">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
								</select>
							</div>
							  
							 
						</div>
					</div>
					<!-- END Regular -->

					
					<!-- Submit -->
					<div class="row items-push">
						<div class="col-lg-7 offset-lg-4">
							<button type="submit" name="add" class="btn btn-primary">Guardar</button>
						</div>
					</div>
					<!-- END Submit -->
				</div>
			</div>
		</div>
	</form>
	<!-- jQuery Validation -->

 
	 
</div>
<!-- END Page Content -->
<?php
$page_title = 'Agregar Usuario';
require_once('../assets/includes/load.php');
 // Checar cual es el nivel permitido de usuario
   page_require_level(1 or 0);
   $groups = find_allgroups('user_groups');    
?>

<?php
  if(isset($_POST['add_user']))
    {
      $req_fields = array('nombre-addusr','username-addusr','password-addusr','val-select2-addusr' );
      validate_fields($req_fields);
      $exist_fields = array('username-addusr');
      validate_exist_fields('users',$exist_fields);

      if(empty($errors))
        {
          $name       = remove_junk($db->escape($_POST['nombre-addusr']));
          $username   = remove_junk($db->escape($_POST['username-addusr']));
          $password   = remove_junk($db->escape($_POST['password-addusr']));
          $user_level = (int)$db->escape($_POST['val-select2-addusr']);
          $password   = sha1($password);
          $query = "INSERT INTO users (";
          $query .="name,username,password,user_level,status";
          $query .=") VALUES (";
          $query .=" '{$name}', '{$username}', '{$password}', '{$user_level}','1'";
          $query .=")";
            if($db->query($query))
              {
                //sucess
                $session->msg('s'," Cuenta de usuario ha sido creada");
                redirect('agregar.php', false);
              } 
            else 
              {
                //failed
                $session->msg('d',' No se pudo crear la cuenta.');
                redirect('agregar.php', false);
              }
        } 
      else 
        {
          $session->msg("d", $errors);
          redirect('agregar.php',false);
        }
    }
?>

<?php include_once('../assets/layouts/headersub.php'); 

date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i a'); ?>

<div id="divXCambiar">
<?php 
  include('agregarcont.php'); 
 ?>
</div>

<?php include_once('../assets/layouts/footersub.php'); ?>

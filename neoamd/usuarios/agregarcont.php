
<!-- Hero -->
<div class="bg-body-light">
	<div class="content content-full">
		<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
			<h1 class="flex-sm-fill h3 my-2">
				Agregar Usuario <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Agregar un nuevo usuario.</small>
			</h1>
			<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-alt">
					<li class="breadcrumb-item">Accesos</li>
					<li class="breadcrumb-item" aria-current="page">
						<a class="link-fx" href="">Agregar Usuario</a>
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">

	 <div class="row">
         <div class="col-md-12">
           <?php echo display_msg($msg); ?>
       </div>
        
        
    </div>

    <br>

	<!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
	<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
	<form class="js-validation" action="agregar.php" method="POST">
		<div class="block">
			<div class="block-header">
				<h3 class="block-title">Agregar USUARIO</h3>
			</div>
			<div class="block-content block-content-full">
				<div class="">
					<!-- Regular -->
					<h2 class="content-heading border-bottom mb-4 pb-2">Llena los siguientes datos.</h2>
					<div class="row items-push">
						<div class="col-lg-4">
							<p class="font-size-sm text-muted">
								Los usuarios son perfiles de personas que utilizaran el sistema.
							</p>
						</div>
						<div class="col-lg-8 col-xl-5">
							<div class="form-group">
								<label for="nombre-addusr">Nombre  <? echo $checkusr; ?> <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="nombre-addusr" name="nombre-addusr" placeholder="Nombre completo">
							</div>
							<div class="form-group">
								<label for="username-addusr">Usuario <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="username-addusr" name="username-addusr" placeholder="Nombre de usuario">
							</div>
							
							<div class="form-group">
                                <label for="password">Contraseña <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" id="password-addusr" name="password-addusr" placeholder="Ingresa una Contraseña">
                            </div>
                            <div class="form-group">
                                <label for="confirm-password-addusr">Confirmar Contraseña <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" id="confirm-password-addusr" name="confirm-password-addusr" placeholder="Confirma tu Contraseña">
                            </div>
                            <div class="form-group">
								<label for="rol">Rol de usuario <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="val-select2-addusr" name="val-select2-addusr" style="width: 100%;" data-placeholder="Selecciona uno..">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<?php foreach ($groups as $group ):?>
									   <option value="<?php echo $group['group_level'];?>"><?php echo ucwords($group['group_name']);?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>
					</div>
					<!-- END Regular -->
					
					<!-- Submit -->
					<div class="row items-push">
						<div class="col-lg-7 offset-lg-4">
							<button type="submit" name="add_user" class="btn btn-primary">Guardar</button>
						</div>
					</div>
					<!-- END Submit -->
				</div>
			</div>
		</div>
	</form>
	<!-- jQuery Validation -->

 
	 
</div>
<!-- END Page Content -->
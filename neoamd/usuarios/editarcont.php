<!-- Hero -->
<div class="bg-body-light">
  <div class="content content-full">
    <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
      <h1 class="flex-sm-fill h3 my-2">
        Editar Usuario <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Modificar los datos de usuario.</small>
      </h1>
      <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-alt">
          <li class="breadcrumb-item">Accesos</li>
          <li class="breadcrumb-item" aria-current="page">
            <a class="link-fx" href="">Editar Usuario</a>
          </li>
        </ol>
      </nav>
    </div>
  </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">

 <div class="row">
  <div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
</div>

<br>

<!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
<form class="js-validation" action="editar.php?id=<?php echo (int)$e_user['id'];?>" method="POST">
  <div class="block">
    <div class="block-header">
      <h3 class="block-title">Editar datos de <?php echo remove_junk(ucwords($e_user['name'])); ?></h3>
    </div>
    <div class="block-content block-content-full">
      <div class="">
        <!-- Regular -->
        <h2 class="content-heading border-bottom mb-4 pb-2">Informacion Basica.</h2>
        <div class="row items-push">
          <div class="col-lg-4">
            <p class="font-size-sm text-muted">
              Los grupos son comunidades o niveles en los usuarios, Entre mas bajo es el nivel mayores son los privilegios.
            </p>
          </div>
          <div class="col-lg-8 col-xl-5">
            <div class="form-group">
              <label for="nombre-ediusr">Nombre <span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="nombre-ediusr" name="nombre-ediusr" value="<?php echo remove_junk(ucwords($e_user['name'])); ?>">
            </div>
            <div class="form-group">
              <label for="username-ediusr">Usuario <span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="username-ediusr" name="username-ediusr" value="<?php echo remove_junk(ucwords($e_user['username'])); ?>">
            </div>
            <div class="form-group">
              <label for="val-select2-ediusr">Rol de usuario <span class="text-danger">*</span></label>
              <select class="js-select2 form-control" id="val-select2-ediusr" name="val-select2-ediusr" style="width: 100%;" data-placeholder="Selecciona uno..">
                <option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
                <?php foreach ($groups as $group ):?>
                 <option <?php if($group['group_level'] === $e_user['user_level']) echo 'selected="selected"';?> value="<?php echo $group['group_level'];?>"><?php echo ucwords($group['group_name']);?></option>
               <?php endforeach;?>
             </select>
           </div>
           <div class="form-group">
            <label for="val-select3-ediusr">Estado <span class="text-danger">*</span></label>
            <select class="js-select2 form-control" id="val-select3-ediusr" name="val-select3-ediusr" style="width: 100%;" data-placeholder="Selecciona uno..">
              <option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
              <option <?php if($e_user['status'] === '1') echo 'selected="selected"';?> value="1"> Activo </option>
              <option <?php if($e_user['status'] === '0') echo 'selected="selected"';?> value="0">Inactivo</option>
            </select>
          </div>

        </div>
      </div>
      <!-- END Regular -->

      <!-- Submit -->
      <div class="row items-push">
        <div class="col-lg-7 offset-lg-4">
          <button type="submit" name="update" class="btn btn-primary">Guardar</button>
        </div>
      </div>
      <!-- END Submit -->
    </div>
  </div>
</div>
</form>
<!-- jQuery Validation -->

<!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
<form class="js-validation" action="editar.php?id=<?php echo (int)$e_user['id'];?>" method="POST">
  <div class="block">
    <div class="block-header">
      <h3 class="block-title">Editar datos de <?php echo remove_junk(ucwords($e_user['name'])); ?></h3>
    </div>
    <div class="block-content block-content-full">
      <div class="">
        <!-- Regular -->
        <h2 class="content-heading border-bottom mb-4 pb-2">Contraseña.</h2>
        <div class="row items-push">
          <div class="col-lg-4">
            <p class="font-size-sm text-muted">
              Los grupos son comunidades o niveles en los usuarios, Entre mas bajo es el nivel mayores son los privilegios.
            </p>
          </div>
          <div class="col-lg-8 col-xl-5">
            <div class="form-group">
              <label for="password">Contraseña <span class="text-danger">*</span></label>
              <input type="password" class="form-control" id="passwordedituser" name="password" placeholder="Ingresa una Contraseña">
            </div>
            <div class="form-group">
              <label for="confirm-password">Confirmar Contraseña <span class="text-danger">*</span></label>
              <input type="password" class="form-control" id="confirm-passwordedituser" name="confirm-password" placeholder="Confirma tu Contraseña">
            </div>
          </div>
        </div>
        <!-- END Regular -->

        <!-- Submit -->
        <div class="row items-push">
          <div class="col-lg-7 offset-lg-4">
            <button type="submit" name="update-pass" class="btn btn-primary">Guardar</button>
          </div>
        </div>
        <!-- END Submit -->
      </div>
    </div>
  </div>
</form>
<!-- jQuery Validation -->

</div>
<!-- END Page Content -->


<script src="../assets/js/pages/validationedituser.min.js"></script>
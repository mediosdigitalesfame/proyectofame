<?php
  $page_title = 'Editar Usuario';
  require_once('../assets/includes/load.php');
  // Checar cual es el nivel permitido de usuario
  page_require_level(1 or 0);
  $e_user = find_by_id('users',(int)$_GET['id']);
  $groups  = find_all('user_groups');
  if(!$e_user){
    $session->msg("d","No Puedes Modificar Este Usuario.");
    redirect('index.php');
  }
?>
 
<?php
//Actualizar Informacion Basica
  if(isset($_POST['update'])){
      $req_fields = array('nombre-ediusr','username-ediusr','val-select2-ediusr','val-select3-ediusr');
      validate_fields($req_fields);
      $exist_fields = array('username-ediusr');
      validate_exist_fields('users',$exist_fields);
    if(empty($errors)){
            $id = (int)$e_user['id'];
            $name = remove_junk($db->escape($_POST['nombre-ediusr']));
            $username = remove_junk($db->escape($_POST['username-ediusr']));
            $level = (int)$db->escape($_POST['val-select2-ediusr']);
            $status   = remove_junk($db->escape($_POST['val-select3-ediusr']));
            $sql = "UPDATE users SET name ='{$name}', username ='{$username}',user_level='{$level}',status='{$status}' WHERE id='{$db->escape($id)}'";
         $result = $db->query($sql);
          if($result && $db->affected_rows() === 1){
            $session->msg('s',"Acount Updated ");
            redirect('editar.php?id='.(int)$e_user['id'], false);
          } else {
            $session->msg('d',' Lo siento no se actualizó los datos.');
            redirect('editar.php?id='.(int)$e_user['id'], false);
          }
    } else {
      $session->msg("d", $errors);
      redirect('editar.php?id='.(int)$e_user['id'],false);
    }
  }
?>
<?php
// Actualizar la contraseña
if(isset($_POST['update-pass'])) {
  $req_fields = array('password');
  validate_fields($req_fields);
  if(empty($errors)){
           $id = (int)$e_user['id'];
     $password = remove_junk($db->escape($_POST['password']));
     $h_pass   = sha1($password);
          $sql = "UPDATE users SET password='{$h_pass}' WHERE id='{$db->escape($id)}'";
       $result = $db->query($sql);
        if($result && $db->affected_rows() === 1){
          $session->msg('s',"Se ha actualizado la contraseña del usuario. ");
          redirect('editar.php?id='.(int)$e_user['id'], false);
        } else {
          $session->msg('d','No se pudo actualizar la contraseña de usuario..');
          redirect('editar.php?id='.(int)$e_user['id'], false);
        }
  } else {
    $session->msg("d", $errors);
    redirect('editar.php?id='.(int)$e_user['id'],false);
  }
}


?>

<?php include_once('../assets/layouts/headersub.php'); 
date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i a'); ?>

<div id="divXCambiar">
<?php 
  include('editarcont.php'); 
 ?>
</div>

<?php include_once('../assets/layouts/footersub.php'); ?>



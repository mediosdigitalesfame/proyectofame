<?php
$page_title = 'Lista de Grupos';
require_once('../assets/includes/load.php');
 // Checkin What level user has permission to view this page
   page_require_level(1);
  $all_groups = find_all('user_groups');
?>

<?php include_once('../assets/layouts/headersub.php'); 

date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i a'); ?>

<div id="divXCambiar">

<?php 
  include('contenido.php'); 
 ?>

</div>

<?php include_once('../assets/layouts/footersub.php'); ?>
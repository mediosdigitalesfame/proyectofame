<ul class="nav-main">

     <li class="nav-main-item">
        <a id="dash" class="nav-main-link" href="../inicio_simple">
            <i class="nav-main-link-icon si si-speedometer"></i>
            <span class="nav-main-link-name">Inicio</span>
        </a>
    </li>

    <li class="nav-main-item">
        <a id="dash" class="nav-main-link" href="../usuarios">
            <i class="nav-main-link-icon si si-speedometer"></i>
            <span class="nav-main-link-name">Usuarios</span>
        </a>
    </li>

 
    <li class="nav-main-heading">Reportes</li>
    <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
            <i class="nav-main-link-icon si si-layers"></i>
            <span class="nav-main-link-name">Mailings</span>
        </a>
        <ul class="nav-main-submenu">
            <li class="nav-main-item">
                <a class="nav-main-link" href="be_pages_generic_blank.html">
                    <span class="nav-main-link-name">Por Agencia</span>
                </a>
            </li>
             
        </ul>
    </li>
    
   
</ul>
<?php include_once('load.php'); ?>
<?php
$req_fields = array('login-username','login-password' );
validate_fields($req_fields);
$username = remove_junk($_POST['login-username']);
$password = remove_junk($_POST['login-password']);

  if(empty($errors)){

    $user = authenticate_v2($username, $password);

        if($user):
           //create session with id
           $session->login($user['id']);
           //Update Sign in time
           updateLastLogIn($user['id']);
           // redirect user to group home page by user level
           if($user['user_level'] === '1'):
             $session->msg("s", "Saludos ".$user['username'].", Bienvenido Administrador.");
             redirect('../../inicio',false);
           elseif ($user['user_level'] === '9'):
              $session->msg("s", "Saludos ".$user['username'].", Bienvenido Usuario Simple. (solo podras ver algunas cosas y no podras modificar nada)");
             redirect('../../inicio_simple',false);
           else:
              $session->msg("s", "Saludos ".$user['username'].", Bienvenido Invitado.");
             redirect('../../inicio',false);
           endif;

        else:
          $session->msg("d", "Username o Password incorrecto.");
          redirect('../../index.php',false);
        endif;

  } else {

     $session->msg("d", $errors);
     redirect('../../index.php',false);
  }

?>

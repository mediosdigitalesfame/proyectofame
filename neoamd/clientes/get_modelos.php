<?php
   require_once('../assets/includes/load.php');
   // Checar cual es el nivel permitido de usuario
   page_require_level(1);
   $agencia_id = filter_input(INPUT_POST, 'agencia_id'); //obtenemos el parametro que viene de ajax
   $models = find_models('agencys_has_models',(int)$agencia_id); 
?>

<option value="">- Seleccione -</option>
<?php foreach($models as $model): //creamos las opciones a partir de los datos obtenidos ?>
<option value="<?= $model['idmodels'] ?>"> <?= ucwords($model['nombre']) ?></option>
<?php endforeach; ?>

 
<?php
$page_title = 'Agregar Cliente';
require_once('../assets/includes/load.php');
 // Checar cual es el nivel permitido de usuario
   page_require_level(1);
   $agencys = find_all('agencys'); 
   $departaments = find_all('departaments'); 
   $models = find_all('models'); 
?>

<?php
  if(isset($_POST['add'])){

   $req_fields = array('nombre-addcli','email-addcli','tel-addcli','select-addcliori','select-addcliniv','select-addcliest','select-addcliage','select-addclidep','msj-addcli');
   validate_fields($req_fields);

   if(find_by_telcliente($_POST['tel-addcli']) === false ){
     $session->msg('d','<b>Error!</b> El Telefono ya existe en la base de datos');
     redirect('agregar.php', false);
   }elseif(find_by_mailcliente($_POST['email-addcli']) === false) {
     $session->msg('d','<b>Error!</b> El Correo ya existe en la base de datos ');
     redirect('agregar.php', false);
   }
   if(empty($errors)){
           $nombre = remove_junk($db->escape($_POST['nombre-addcli']));
           $correo = remove_junk($db->escape($_POST['email-addcli']));
           $telefono = remove_junk($db->escape($_POST['tel-addcli']));
           $fecha = remove_junk($db->escape($_POST['fecha-addcli']));
           $origen = remove_junk($db->escape($_POST['select-addcliori']));
           $nivel = remove_junk($db->escape($_POST['select-addcliniv']));
           $estado = remove_junk($db->escape($_POST['select-addcliest']));
           $agencia = remove_junk($db->escape($_POST['select-addcliage']));
           $modelo = remove_junk($db->escape($_POST['select-addclimod']));
           $departamento = remove_junk($db->escape($_POST['select-addclidep']));
           $mensaje = remove_junk($db->escape($_POST['msj-addcli']));

        $query  = "INSERT INTO costumers (";
        $query .="nombrecostumers,origen,statuscostumers,nivel,fecha,correo,telefono,mensaje,agencys_idagencys,model,departaments_iddepartaments";
        $query .=") VALUES (";
        $query .=" '{$nombre}', '{$origen}','{$estado}','{$nivel}','{$fecha}','{$correo}','{$telefono}','{$mensaje}','{$agencia}','{$modelo}','{$departamento}'";
        $query .=")";
        if($db->query($query)){
          //sucess 
          $session->msg('s',"El Cliente ha sido registrado! ");
          redirect('agregar.php', false);
        } else {
          //failed
          $session->msg('d','Lamentablemente no se pudo registrar el cliente!');
          redirect('agregar.php', false);
        }
   } else {
     $session->msg("d", $errors);
      redirect('agregar.php',false);
   }
 }
?>

<?php include_once('../assets/layouts/headersub.php'); 
date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$hoy=date('Y-m-d g:i:s'); ?>

<div id="divXCambiar">
<?php 
  include('agregarcont.php'); 
 ?>
</div>

<!-- javascript para sacar los modelos de agencias -->
    <script type="text/javascript">
      $(document).ready(function(){

        var modelos = $('#select-addclimod');
        
        //Ejecutar accion al cambiar de opcion en el select de las bandas
        $('#select-addcliage').change(function(){
          var agencia_id = $(this).val(); //obtener el id seleccionado
          
          if(agencia_id !== ''){ //verificar haber seleccionado una opcion valida
            
            /*Inicio de llamada ajax*/
            $.ajax({
              data: {agencia_id:agencia_id}, //variables o parametros a enviar, formato => nombre_de_variable:contenido
              dataType: 'html', //tipo de datos que esperamos de regreso
              type: 'POST', //mandar variables como post o get
              url: 'get_modelos.php' //url que recibe las variables
            }).done(function(data){ //metodo que se ejecuta cuando ajax ha completado su ejecucion             
              
              modelos.html(data); //establecemos el contenido html de discos con la informacion que regresa ajax             
              modelos.prop('disabled', false); //habilitar el select
            });
            /*fin de llamada ajax*/
            
          }else{ //en caso de seleccionar una opcion no valida
            modelos.val(''); //seleccionar la opcion "- Seleccione -", osea como reiniciar la opcion del select
            modelos.prop('disabled', true); //deshabilitar el select
          }    
        });

        
 
      });

    </script>   

<?php include_once('../assets/layouts/footersub.php'); ?>
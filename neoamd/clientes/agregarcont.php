
<!-- Hero -->
<div class="bg-body-light">
	<div class="content content-full">
		<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
			<h1 class="flex-sm-fill h3 my-2">
				Agregar Cliente <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Agregar un nuevo grupo de usuarios.</small>
			</h1>
			<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-alt">
					<li class="breadcrumb-item">Accesos</li>
					<li class="breadcrumb-item" aria-current="page">
						<a class="link-fx" href="">Agregar Cliente</a>
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
	 <div class="row">
         <div class="col-md-12">
           <?php echo display_msg($msg); ?>
       </div>
</div>

    <br>

	<!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
	<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
	<form class="js-validation" id="formulario" action="agregar.php" method="POST">
		<div class="block">
			<div class="block-header">
				<h3 class="block-title">Agregar CLIENTE</h3>
			</div>
			<div class="block-content block-content-full">
				<div class="">
					<!-- Regular -->
					<h2 class="content-heading border-bottom mb-4 pb-2">Llena los siguientes datos.</h2>
					<div class="row items-push">
						<div class="col-lg-4">
							<p class="font-size-sm text-muted">
								Los clientes son personas interesadas en nuestros productos.
							</p>
						</div>
						<div class="col-lg-8 col-xl-5">
							<div class="form-group">
								<label for="nombre-addcli">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="nombre-addcli" name="nombre-addcli" placeholder="Ingresa un Nombre.." value=' '>
							</div>

							<div class="form-group">
								<label for="email-addcli">Correo <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="email-addcli" name="email-addcli" placeholder="Correo Electronico">
							</div>

							<div class="form-group">
								<label for="group-name">Telefono <span class="text-danger">*</span></label>
								<input type="text" class="js-masked-phone form-control" id="tel-addcli" name="tel-addcli" placeholder="(999) 999-9999">
							</div>

							<div class="form-group">
								<label for="fecha-addcli">Fecha <span class="text-danger">*</span></label>
								<input type="text" readonly class="form-control" id="fecha-addcli" name="fecha-addcli" value="<?php echo $hoy; ?>" >
							</div>

							<div class="form-group">
								<label for="val-select2-addgpr">Origen <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addcliori" name="select-addcliori" style="width: 100%;" data-placeholder="Selecciona un Origen">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<option value="3">Web</option>
									<option value="2">Facebook</option>
									<option value="1">Google</option>
                                    <option value="0">Directo</option>
								</select>
							</div>

							<div class="form-group">
								<label for="select-addcliniv">Nivel <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addcliniv" name="select-addcliniv" style="width: 100%;" data-placeholder="Selecciona un Nivel">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<option value="1">Cliente</option>
                                    <option value="0">Lead</option>
								</select>
							</div>
							 
							<div class="form-group">
								<label for="select-addcliest">Status <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addcliest" name="select-addcliest" style="width: 100%;" data-placeholder="Selecciona un Status">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
								</select>
							</div>

							
                            <div class="form-group">
								<label for="select-addcliage">Agencia <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addcliage" name="select-addcliage" style="width: 100%;" data-placeholder="Selecciona una Agencia">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<?php foreach ($agencys as $agency ):?>
									   <option value="<?php echo $agency['idagencys'];?>"><?php echo ucwords($agency['nombreagencys']);?></option>
									<?php endforeach;?>
								</select>
							</div>

							<div class="form-group">
								<label for="select-addcliage">Modelo <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addclimod" disabled="" name="select-addclimod" style="width: 100%;" data-placeholder="Selecciona un Modelo">
									 
								</select>
							</div>

							<div class="form-group">
								<label for="select-addclidep">Departamento <span class="text-danger">*</span></label>
								<select class="js-select2 form-control" id="select-addclidep" name="select-addclidep" style="width: 100%;" data-placeholder="Selecciona un Departamento">
									<option></option><!-- Necesario para que el atributo de marcador de posición de datos funcione con Select2 plugin -->
									<?php foreach ($departaments as $departament ):?>
									   <option value="<?php echo $departament['iddepartaments'];?>"><?php echo ucwords($departament['nombredepartaments']);?></option>
									<?php endforeach;?>
								</select>
							</div>

							<div class="form-group">
							 	<label for="msj-addcli">Mensaje <span class="text-danger">*</span></label>
                                <textarea class="js-maxlength form-control" id="msj-addcli" name="msj-addcli" rows="5" maxlength="145" placeholder="Escribe aqui tu mensaje.." data-always-show="true"></textarea>
                            </div>
							  
							 
						</div>
					</div>
					<!-- END Regular -->

					
					<!-- Submit -->
					<div class="row items-push">
						<div class="col-lg-7 offset-lg-4">
							<button type="submit" id="add" name="add" class="btn btn-primary">Guardar</button>
						</div>
					</div>
					<!-- END Submit -->
				</div>
			</div>
		</div>
	</form>
	<!-- jQuery Validation -->

 
	 
</div>
<!-- END Page Content -->





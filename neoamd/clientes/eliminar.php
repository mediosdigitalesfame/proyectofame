<?php
  require_once('../assets/includes/load.php');
  // Checar cual es el nivel permitido de usuario
   page_require_level(1);
?>
<?php
  $delete_id = delete_by_id('user_groups',(int)$_GET['id']);
  if($delete_id){
      $session->msg("s","Grupo eliminado");
      redirect('index.php');
  } else {
      $session->msg("d","Eliminación falló");
      redirect('index.php');
  }
?>

  <!-- Hero -->
  <div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Clientes <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Administración de Clientes del Grupo.</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Clientes</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="">Administrar CLientes</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">

    <div class="row">
         <div class="col-md-12">
           <?php echo display_msg($msg); ?>
       </div>
       <div class="col-md-10"></div>
       <div align="right" class="col-md-2">
            <a href="agregar.php" class="btn btn-success"> Agregar CLIENTE</a>
            
       </div>
    </div>

    <br>

    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Clientes <small>Existentes</small></h3>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 70px;">#</th>
                        <th>Nombre del Cliente</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Correo</th>
                        <th class="d-none d-sm-table-cell" style="width: 10%;">Telefono</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Agencia</th>
                        <th class="d-none d-sm-table-cell" style="width: 10%;">Departamento</th>
                        <th class="d-none d-sm-table-cell" style="width: 5%;">Estado</th>
                        <th class="d-none d-sm-table-cell" style="width: 5%;">Nivel</th>
                        <th style="width: 10%;">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($all_costumers as $a_costumer): ?>
                        <tr>
                            <td class="text-center font-size-sm"><?php echo count_id();?></td>
                            <td class="font-w600 font-size-sm">
                                <a href="be_pages_generic_blank.html"><?php echo remove_junk(ucwords($a_costumer['nombrecostumers']))?></a>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk($a_costumer['correo'])?>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_costumer['telefono']))?>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_costumer['nombreagencys']))?>
                            </td>
                             <td class="d-none d-sm-table-cell font-size-sm">
                                <?php echo remove_junk(ucwords($a_costumer['nombredepartaments']))?>
                            </td>
                            <td class="d-none d-sm-table-cell">
                                <?php if($a_costumer['statuscostumers'] === '1'): ?></span>
                                    <span class="badge badge-info"><?php echo "Activo"; ?></span>
                                <?php else: ?>
                                    <span class="badge badge-danger"><?php echo "Inactivo"; ?></span>
                                <?php endif;?>
                            </td>
                             <td class="d-none d-sm-table-cell">
                                <?php if($a_costumer['nivel'] === '1'): ?></span>
                                    <span class="badge badge-info"><?php echo "Cliente"; ?></span>
                                <?php else: ?>
                                    <span class="badge badge-danger"><?php echo "Lead"; ?></span>
                                <?php endif;?>
                            </td>
                            <td>
                                <a href="editar.php?id=<?php echo (int)$a_group['id'];?>" class="btn btn-sm btn-info" data-toggle="tooltip" title="Editar">
                                    <i class="fas fa-pencil-alt fa-1x"></i>
                                </a>

                                <a href="eliminar.php?id=<?php echo (int)$a_group['id'];?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                    <i class="fas fa-trash-alt fa-1x"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>

                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->